public class Employee extends User{
    double salary;
    String position;
    String department;
    Address address;

    Employee(String fullName, int age, String gender, String email, String country, double salary, String position, String department, Address address) {
        super(fullName, age, gender, email, country);
        this.salary = salary;
        this.position = position;
        this.department = department;
        this.address = address;
    }

    double getSalary() { return salary; }
    String getPosition() { return position; }
    String getDepartment() { return department; }
    Address getAddress() { return address; }

    void setSalary(double salary) { this.salary = salary; }
    void setPosition(String position) { this.position = position; }
    void setDepartment(String department) { this.department = department; }
    void setAddress(Address address) { this.address = address; }

    @Override
    public String toString(){
        return fullName + " (співробітник)";
    }
}
