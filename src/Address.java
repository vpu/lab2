public class Address {
    String city;
    String street;
    String house;
    String room;

    Address(String city, String street, String house, String room) {
        this.city = city;
        this.street = street;
        this.house = house;
        this.room = room;
    }

    String getCity() { return city; }
    String getStreet() { return street; }
    String getHouse() { return house; }
    String getRoom() { return room; }

    void setCity(String city) { this.city = city; }
    void setStreet(String street) { this.street = street; }
    void setHouse(String house) { this.house = house; }
    void setRoom(String room) { this.room = room; }
}
