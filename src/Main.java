import java.util.*;
import java.util.stream.*;

public class Main {
    public static void main(String[] args) {
        List<User> users = new ArrayList<>();
        users.add(new User("Іван Іванов", 25, "Чоловік", "ivanov@gmail.com", "Україна"));
        users.add(new Employee("Петро Петров", 30, "Чоловік", "petrov@gmail.com", "Україна", 50000, "Розробник", "IT", new Address("Київ", "Хрещатик", "1", "1")));
        users.add(new User("Олександр Олександров", 27, "Чоловік", "olexandrov@gmail.com", "Україна"));
        users.add(new Employee("Марія Марієнко", 28, "Жінка", "mariivna@gmail.com", "Україна", 60000, "Дизайнер", "Дизайн", new Address("Львів", "Свободи", "2", "2")));
        users.add(new User("Анна Андрієнко", 26, "Жінка", "andriivna@gmail.com", "Україна"));
        users.add(new Employee("Юрій Юрієвич", 16, "Чоловік", "yurievich@gmail.com", "Україна", 70000, "Менеджер", "Менеджмент", new Address("Одеса", "Дерибасівська", "3", "3")));
        users.add(new User("Віктор Вікторський", 27, "Чоловік", "viktorovich@gmail.com", "Україна"));
        users.add(new Employee("Наталія Наталієнко", 32, "Жінка", "nataliivna@gmail.com", "Україна", 80000, "Дизайнер", "Дизайн", new Address("Харків", "Сумська", "4", "4")));
        users.add(new User("Сергій Сергієнко", 33, "Чоловік", "sergiiovych@gmail.com", "Україна"));
        users.add(new Employee("Олена Зіневич", 17, "Жінка", "olenivna@gmail.com", "Німеччина", 90000, "Тестувальник", "Тестування", new Address("Берлін", "Свободи", "5", "5")));

        List<User> onlyUsers = users.stream()
                .filter(u -> !(u instanceof Employee))
                .collect(Collectors.toList());
        System.out.println("Тільки користувачі: " + onlyUsers);

        double avgAge = users.stream()
                .filter(u -> u instanceof Employee)
                .map(u -> (Employee) u)
                .filter(e -> e.getAge() < 18)
                .mapToInt(User::getAge)
                .average()
                .orElse(0.0);
        System.out.println("Середній вік неповнолітніх співробітників: " + avgAge);


        List<Employee> developersFromKyiv = users.stream()
                .filter(u -> u instanceof Employee)
                .map(u -> (Employee) u)
                .filter(e -> e.getPosition().equals("Розробник") && e.getAddress().getCity().equals("Київ"))
                .collect(Collectors.toList());
        System.out.println("Розробники з Києва: " + developersFromKyiv);

        List<String> designersCities = users.stream()
                .filter(u -> u instanceof Employee)
                .map(u -> (Employee) u)
                .filter(e -> e.getPosition().equals("Дизайнер"))
                .map(e -> e.getAddress().getCity())
                .collect(Collectors.toList());
        System.out.println("Міста дизайнерів: " + designersCities);

        List<User> gmailUsers = users.stream()
                .filter(u -> u.getEmail().endsWith("gmail.com"))
                .collect(Collectors.toList());
        System.out.println("Користувачі з Gmail: " + gmailUsers);

        List<String> emails = users.stream()
                .filter(u -> u instanceof Employee)
                .map(u -> (Employee) u)
                .filter(e -> e.getAge() <= 30 && e.getGender().equals("Жінка") && e.getCountry().equals("Україна"))
                .map(User::getEmail)
                .collect(Collectors.toList());
        System.out.println("Електронні адреси жінок: " + emails);

        Map<String, Double> avgSalaryByDepartment = users.stream()
                .filter(u -> u instanceof Employee)
                .map(u -> (Employee) u)
                .collect(Collectors.groupingBy(Employee::getDepartment, Collectors.averagingDouble(Employee::getSalary)));
        System.out.println("Середня зарплата по відділам: " + avgSalaryByDepartment);
    }
}