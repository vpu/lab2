public class User
{
    String fullName;
    int age;
    String gender;
    String email;
    String country;

    User(String fullName, int age, String gender, String email, String country) {
        this.fullName = fullName;
        this.age = age;
        this.gender = gender;
        this.email = email;
        this.country = country;
    }

    String getFullName() { return fullName; }
    int getAge() { return age; }
    String getGender() { return gender; }
    String getEmail() { return email; }
    String getCountry() { return country; }

    void setFullName(String fullName) { this.fullName = fullName; }
    void setAge(int age) { this.age = age; }
    void setGender(String gender) { this.gender = gender; }
    void setEmail(String email) { this.email = email; }
    void setCountry(String country) { this.country = country; }

    @Override
    public String toString(){
        return fullName;
    }
}